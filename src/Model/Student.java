package Model;

import java.io.Serializable;

public class Student implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String email;
	private String tel;
	private String sclass;
	private String note;

	public Student() {
	}

	public Student(int id, String name, String email, String tel, String sclass, String note) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.tel = tel;
		this.sclass = sclass;
		this.note = note;
	}

	public int getId() {
		return id;
	}

	public void setId(int i) {
		this.id = i;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getSclass() {
		return sclass;
	}

	public void setSclass(String sclass) {
		this.sclass = sclass;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String showStudent() {
		return String.format("%-5s%-20s%-30s%-15s%-5s%-25s", id, name, email, tel, sclass, note);
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", email=" + email + ", tel=" + tel + ", sclass=" + sclass
				+ ", note=" + note + "]";
	}

}
