package Model;

public class ClassInfo {
	private int id;
	private String nameClass;
	private String description;

	public ClassInfo() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameClass() {
		return nameClass;
	}

	public void setNameClass(String nameClass) {
		this.nameClass = nameClass;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ClassInfo(int id, String nameClass, String description) {
		super();
		this.id = id;
		this.nameClass = nameClass;
		this.description = description;
	}

}
