
package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.AccountDAO;
import DAO.ClassDAO;
import DAO.StudentDAO;
import Logic.ValidateAccount;
import Logic.validateValue;
import Model.Account;
import Model.ClassInfo;
import Model.Student;





@WebServlet("/editstudent")
public class EditStudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	StudentDAO studentdao = new StudentDAO();
	ClassDAO clsdao = new ClassDAO();
	validateValue vld = new validateValue();

	public EditStudentController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id= request.getParameter("studentId");
		Student student = new Student();
		List<ClassInfo> listclass = new ArrayList<>();
		try {
			student = studentdao.getStudentById(id);
			listclass = clsdao.getListClass();
			request.setAttribute("listclass", listclass);
			request.setAttribute("student", student);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/editstudent.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			List<String> listErr = new ArrayList<>();
			ClassInfo cls = new ClassInfo();
			String name = request.getParameter("name");
			if(vld.checkIsEmpty(name) == false) {
				listErr.add("Chưa nhập name");
			}
			String email = request.getParameter("email");
			if(vld.checkIsEmpty(email) == false) {
				listErr.add("Chưa nhập email");
			}
			if(vld.checkEmail(email) == false) {
				listErr.add("Email chưa đúng định dạng");
			}
			String sclass = request.getParameter("sclass");
			if(vld.checkIsEmpty(sclass) == false) {
				listErr.add("Chưa nhập class");
			}
			String tel = request.getParameter("tel");
			if(vld.checkIsEmpty(tel) == false) {
				listErr.add("Chưa nhập tel");
			}
			String note = request.getParameter("note");
			cls = clsdao.getClassIdbyName(sclass);
			int idClass = cls.getId();
			
			if (listErr.isEmpty()) {
				studentdao.updateStudentById(id, name, email, idClass, tel,note );
				response.sendRedirect("liststudent");
			} else {
				request.setAttribute("listError", listErr);
				this.doGet(request, response);
			}			
		} catch (Exception e) {
			System.out.println("Loi!!! ");
		}
	}


}
