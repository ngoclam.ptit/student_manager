
package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.AccountDAO;
import Logic.ValidateAccount;
import Logic.validateValue;
import Model.Account;





@WebServlet("/editaccount")
public class EditAccountController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AccountDAO accdao = new AccountDAO();
	validateValue vld = new validateValue();

	public EditAccountController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id= request.getParameter("accId");
		Account account = new Account();
		try {
			account = accdao.getAccountById(id);
			request.setAttribute("account", account);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/editaccount.jsp");
			dispatcher.forward(request, response);
		} catch (ClassNotFoundException | IOException | SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			List<String> listErr = new ArrayList<>();
			String id = request.getParameter("id");		
			
			String username = request.getParameter("username");
			if(vld.checkIsEmpty(username) == false) {
				listErr.add("Chưa nhập username");
			}
			String password = request.getParameter("password");
			if(vld.checkIsEmpty(password) == false) {
				listErr.add("Chưa nhập password");
			}
			String name = request.getParameter("name");
			if(vld.checkIsEmpty(name) == false) {
				listErr.add("Chưa nhập name");
			}
			String mail = request.getParameter("email");
			if(vld.checkIsEmpty(mail) == false) {
				listErr.add("Chưa nhập mail");
			}			
			if(vld.checkEmail(mail) == false) {
				listErr.add("Nhập email chưa đúng định dạng");
			}
			if (listErr.isEmpty()) {
				accdao.updateAccountById(id, username, password, name, mail );
				response.sendRedirect("liststudent");
			} else {
				request.setAttribute("listError", listErr);
				this.doGet(request, response);
			}
			
			accdao.updateAccountById(id, username, password, name, mail );
			response.sendRedirect("liststudent");
		} catch (Exception e) {
			System.out.println("Loi!!! ");
		}
	}

}
