
package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.AccountDAO;
import Logic.ValidateAccount;
import Model.Account;





@WebServlet("/deleteaccount")
public class DeleteAccountController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AccountDAO accdao = new AccountDAO();

	public DeleteAccountController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id= request.getParameter("accId");
		try {
			accdao.deleteAccountById(id);
			response.sendRedirect("liststudent");
		} catch (Exception e) {
			System.out.println("Loi!!! ");
		}
		
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

