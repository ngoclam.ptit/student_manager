
package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ClassDAO;
import DAO.StudentDAO;
import Logic.AccountLogic;
import Logic.ValidateAccount;
import Model.Account;
import Model.ClassInfo;
import Model.Student;



@WebServlet("/liststudent")
public class ListStudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AccountLogic accLogic = new AccountLogic();
	StudentDAO studao = new StudentDAO();
	ClassDAO classdao = new ClassDAO();
	
	
	public ListStudentController() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	/*
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			

			List<Account> listAcc = new ArrayList<>();
			List<Student> listStu = new ArrayList<>();
			List<ClassInfo> listClass = new ArrayList<>();
			try {
				listAcc = accLogic.getListAcc();			
				request.setAttribute("listAcc", listAcc);
				
				listStu = studao.getListStudent();
				request.setAttribute("listStu", listStu);
				
				listClass = classdao.getListClass();
				request.setAttribute("listClass", listClass);

				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/liststudent.jsp");
				dispatcher.forward(request, response);
			} catch (Exception e) {

			}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		doGet(request, response);
	}
}
