

package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.AccountDAO;
import DAO.ClassDAO;
import Logic.ValidateAccount;
import Logic.validateValue;
import Model.Account;





@WebServlet("/addclass")
public class AddClassController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ClassDAO classdao = new ClassDAO();
	validateValue vld = new validateValue();

	public AddClassController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
			RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/addclass.jsp");
			dispatcher.forward(request, response);		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<String> listErr = new ArrayList<>();
			String nameClass = request.getParameter("nameClass");
			if(vld.checkIsEmpty(nameClass) == false) {
				listErr.add("Chưa nhập nameClass");
			}
			String description = request.getParameter("description");
			if(vld.checkIsEmpty(description) == false) {
				listErr.add("Chưa nhập description");
			}

			if (listErr.isEmpty()) {
				classdao.addAccount( nameClass, description );
				response.sendRedirect("liststudent");
			} else {
				request.setAttribute("listError", listErr);
				this.doGet(request, response);
			}
				

			
		} catch (Exception e) {
			System.out.println("Loi!!! ");
		}
	}

}
