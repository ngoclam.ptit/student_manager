package Logic;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DAO.AccountDAO;
import Model.Account;

public class ValidateAccount {
	static AccountDAO accdao = new AccountDAO();

	public static List<String> validatelogin(String username, String password)
			throws ClassNotFoundException, IOException, SQLException {
		List<String> listError = new ArrayList<>();
		listError = accdao.checkExistedLoginName(username, password);
		return listError;
	}

}
