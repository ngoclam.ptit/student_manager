package Logic;

import java.util.ArrayList;
import java.util.List;

import java.util.regex.Pattern;

public class validateValue {
	List<String> lissErr = new ArrayList<String>();
	
	public boolean checkIsEmpty(String s) {
		if (s.isEmpty() || s == null) {
			return false;		
		}
		return true;
	}
	
	public boolean checkEmail(String email) {
        String EMAIL_PATTERN = 
            "^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";          
        return Pattern.matches(EMAIL_PATTERN, email);
	}
}
