package Logic;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DAO.AccountDAO;
import Model.Account;

public class AccountLogic {
	static AccountDAO accdao = new AccountDAO();

	public static List<Account> getListAcc()
			throws ClassNotFoundException, IOException, SQLException {
		List<Account> listAcc = new ArrayList<>();
		listAcc = accdao.getListAccount();
		return listAcc;
	}
}
