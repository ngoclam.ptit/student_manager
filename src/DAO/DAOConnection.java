package DAO;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.io.IOException;
import java.sql.Connection;

public class DAOConnection {
	private static String DB_URL = "jdbc:mysql://localhost:3306/qldt?serverTimezone=UTC";
	private static String USER_NAME = "root";
	private static String PASSWORD = "root123";

	public static Connection getMySQLConnection() throws IOException, SQLException, ClassNotFoundException {
		Connection conn = null;
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
		return conn;
	}
	

//	public static Connection getConnection(String dbURL, String userName, String password) {
//		Connection conn = null;
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//			conn = DriverManager.getConnection(dbURL, userName, password);
//			System.out.println("connect successfully!");
//		} catch (Exception ex) {
//			System.out.println("connect failure!");
//			ex.printStackTrace();
//		}
//		return conn;
//	}
}
