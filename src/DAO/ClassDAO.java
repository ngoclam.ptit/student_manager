package DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Account;
import Model.ClassInfo;

public class ClassDAO {
	DAOConnection DAOcon = new DAOConnection();
	
	public List<ClassInfo> getListClass() throws ClassNotFoundException, IOException, SQLException {
		List<ClassInfo> listClass = new ArrayList<>();
		Connection con = DAOcon.getMySQLConnection();
		String sql = "SELECT * from tbl_class ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {	
				ClassInfo cls = new ClassInfo();
				cls.setId(rs.getInt(1));
				cls.setNameClass(rs.getString(2));
				cls.setDescription(rs.getString(3));

				listClass.add(cls);
			}
			return listClass;
		} finally {

		}
	}
	
	public ClassInfo getClassIdbyName(String nameClass) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		ClassInfo cls = new ClassInfo();
		String sql = "SELECT * from tbl_class WHERE nameClass = ? ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, nameClass);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {	
				cls.setId(rs.getInt(1));
				cls.setNameClass(rs.getString(2));
				cls.setDescription(rs.getString(3));			}
		} finally {

		}
		return cls;
	}

	public void addAccount(String nameClass, String description) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		String sql = "INSERT INTO tbl_class (nameClass, descripton) VALUES (?, ?)";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, nameClass);
			ps.setString(2, description);

			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public ClassInfo getClassById(String id) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		ClassInfo cls = new ClassInfo();
		String sql = "SELECT * from tbl_class WHERE id = ? ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {	
				cls.setId(rs.getInt(1));
				cls.setNameClass(rs.getString(2));
				cls.setDescription(rs.getString(3));
				}
		} finally {

		}
		return cls;
	}

	public void updateClassById(String id, String nameClass, String description) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		String sql = "UPDATE tbl_class SET nameClass = ? , descripton = ? WHERE id = ? ";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, nameClass);
			ps.setString(2, description);
			int id1 = Integer.parseInt(id);
			ps.setInt(3, id1);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	public void deleteClassById(String id) throws ClassNotFoundException, IOException, SQLException {		
		Connection con = DAOcon.getMySQLConnection();

		String sql = "DELETE FROM tbl_class WHERE id = ?; ";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			int id1 = Integer.parseInt(id);
			ps.setInt(1, id1);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	
}
