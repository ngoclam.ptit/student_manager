package DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Account;
import Model.Student;

public class StudentDAO {
	DAOConnection DAOcon = new DAOConnection();
	
	public List<Student> getListStudent() throws ClassNotFoundException, IOException, SQLException {
		List<Student> listStu = new ArrayList<Student>();
		Connection con = DAOcon.getMySQLConnection();
		String sql = "select st.id, st.fullname, st.email, cl.nameClass, st.tel, st.note FROM tbl_student st INNER JOIN tbl_class cl ON st.class_id = cl.id ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();		
			while (rs.next()) {	
				Student std = new Student();
				std.setId(rs.getInt(1));
				std.setName(rs.getString(2));
				std.setEmail(rs.getString(3));
				std.setSclass(rs.getString(4));
				std.setTel(rs.getString(5));
				std.setNote(rs.getString(6));
				listStu.add(std);
			}
			return listStu;
		} finally {

		}
	}

	public Student getStudentById(String id) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		Student student = new Student();
		String sql = "select st.id, st.fullname, st.email, cl.nameClass, st.tel, st.note FROM tbl_student st INNER JOIN tbl_class cl ON st.class_id = cl.id WHERE st.id = ? ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {	
				student.setId(rs.getInt(1));
				student.setName(rs.getString(2));
				student.setEmail(rs.getString(3));
				student.setSclass(rs.getString(4));
				student.setTel(rs.getString(5));
				student.setNote(rs.getString(6));
			}
		} finally {

		}
		return student;
	}

	public void updateStudentById(String id, String name, String email, int idClass, String tel, String note) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		System.out.println(id + name + email + idClass + tel + note );
		String sql = "UPDATE tbl_student SET fullname = ? , email =? , class_id= ?,  tel =?, note = ?	WHERE id = ? ";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, email);
			ps.setInt(3, idClass);
			ps.setString(4, tel);
			ps.setString(5, note );
			int idStr = Integer.parseInt(id);
			ps.setInt(6, idStr);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		
	}

	public void deleteStudentById(String id) throws ClassNotFoundException, IOException, SQLException {	
		Connection con = DAOcon.getMySQLConnection();
		System.out.println(id);
		String sql = "DELETE FROM tbl_student WHERE id = ?; ";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			int id1 = Integer.parseInt(id);
			ps.setInt(1, id1);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public void AddStudent(String name, String email, int idClass, String tel, String note) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		String sql = "INSERT INTO tbl_student (fullname, email, class_id, tel, note) VALUES (? , ? , ? , ?, ?)";
		System.out.println(name + email + idClass + tel + note);
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, email);
			ps.setInt(3, idClass);
			ps.setString(4, tel);
			ps.setString(5, note);
			row = ps.executeUpdate();
			System.out.println(row);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
