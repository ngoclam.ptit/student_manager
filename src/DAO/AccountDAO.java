package DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Account;

public class AccountDAO {
	DAOConnection DAOcon = new DAOConnection();
	List<Account> listaccount = new ArrayList();
	List<String> messerr = new ArrayList();

	public List<String> checkExistedLoginName(String username, String pass)
			throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		String sql = "SELECT * from tbl_account WHERE username=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			Account acc = new Account();
			if (rs.next()) {				
				acc.setId(rs.getInt(1));
				acc.setUsername(rs.getString(2));
				acc.setPassword(rs.getString(3));
				acc.setName(rs.getString(4));
				acc.setEmail(rs.getString(5));
			}
				if (acc.getName() == null) {
					messerr.add("Không tồn tại tài khoản!!! ");
				} else {
					if (acc.getPassword().equals(pass)) {
						messerr = null;
					} else {
						messerr.add("Sai mật khẩu!!! ");
					}
				}
		} finally {

		}
		return messerr;
		
	}
	
	public List<Account> getListAccount() throws ClassNotFoundException, IOException, SQLException {
		List<Account> listAcc = new ArrayList<>();
		Connection con = DAOcon.getMySQLConnection();
		String sql = "SELECT * from tbl_account ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {	
				Account acc = new Account();
				acc.setId(rs.getInt(1));
				acc.setUsername(rs.getString(2));
				acc.setPassword(rs.getString(3));
				acc.setName(rs.getString(4));
				acc.setEmail(rs.getString(5));
				listAcc.add(acc);
			}
			return listAcc;
		} finally {

		}
	}
	
	public Account getAccountById(String id) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		System.out.println(id);
		Account acc = new Account();
		String sql = "SELECT * from tbl_account WHERE id = ? ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			System.out.println();
			while (rs.next()) {	
				acc.setId(rs.getInt(1));
				acc.setUsername(rs.getString(2));
				acc.setPassword(rs.getString(3));
				acc.setName(rs.getString(4));
				acc.setEmail(rs.getString(5));
			}
		} finally {

		}
		return acc;
	}
	
	
	public void updateAccountById(String id, String username, String password, String name, String mail) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		String sql = "UPDATE tbl_account SET username = ? , password = ? , nameAcc = ?,  mailAcc = ?	WHERE id = ? ";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, name);
			ps.setString(4, mail);
			int id1 = Integer.parseInt(id);
			ps.setInt(5, id1);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}

	public void deleteAccountById(String id) throws ClassNotFoundException, IOException, SQLException {		
		Connection con = DAOcon.getMySQLConnection();
		System.out.println(id);
		String sql = "DELETE FROM tbl_account WHERE id = ?; ";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			int id1 = Integer.parseInt(id);
			ps.setInt(1, id1);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	public void addAccount(String username, String password, String name, String mail) throws ClassNotFoundException, IOException, SQLException {
		Connection con = DAOcon.getMySQLConnection();
		String sql = "INSERT INTO tbl_account (username, password, nameAcc, mailAcc) VALUES (?, ?, ?, ?)";
		int row =0;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, name);
			ps.setString(4, mail);
			row = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
