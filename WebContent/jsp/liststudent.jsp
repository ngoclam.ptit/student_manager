<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
<head>
<meta charset="utf-8">
<title>ListUser</title>
</head>
<body>
	<h1 style="text-align: center;">Quản lý đào tạo</h1>
	<table border="1" style="margin: auto;">
		<tr>
			<th colspan="5">Tài Khoản giáo vụ</th>
		</tr>
		<tr>
			<td>ID</td>
			<td>Username</td>
			<td>Password</td>
			<td>Name</td>
			<td>Mail</td>

		</tr>

		<c:forEach items="${listAcc}" var="acc">
			<tr>
				<td>${acc.id}</td>
				<td>${acc.username}</td>
				<td>${acc.password}</td>
				<td>${acc.name}</td>
				<td>${acc.email}</td>
				<td><a href="editaccount?accId=${acc.id}">Sửa</td>
				<td><a href="deleteaccount?accId=${acc.id}">Xóa</td>
			</tr>
		</c:forEach>
	</table>
	<br>
	<div style="margin-left: 45%;">
		<button>
			<a href="addaccount">Thêm account</a>
		</button>
	</div>

	<br>
	<br>
	<br>

	<table border="1" style="margin: auto;">
		<tr>
			<th colspan="6">Danh sách Sinh Viên</th>
		</tr>
		<tr>
			<td>ID</td>
			<td>Tên sinh viên</td>
			<td>Email</td>
			<td>Lớp</td>
			<td>Số điện thoại</td>
			<td>Ghi chú</td>
		</tr>

		<c:forEach items="${listStu}" var="student">
			<tr>
				<td>${student.id}</td>
				<td>${student.name}</td>
				<td>${student.email}</td>
				<td>${student.sclass}</td>
				<td>${student.tel}</td>
				<td>${student.note}</td>
				<td><a href="editstudent?studentId=${student.id}">Sửa</td>
				<td><a href="deletestudent?studentId=${student.id}">Xóa</td>
			</tr>
		</c:forEach>
	</table>
	<br>
	<div style="margin-left: 45%;">
		<button>
			<a href="addstudent">Thêm Sinh viên</a>
		</button>
	</div>

	<br>

	<table border="1" style="margin: auto;">
		<tr>
			<th colspan="3">Danh sách Lớp</th>
		</tr>
		<tr>
			<td>ID</td>
			<td>Tên Lớp</td>
			<td>Mô Tả</td>

		</tr>
		<c:forEach items="${listClass}" var="cl">
			<tr>
				<td>${cl.id}</td>
				<td>${cl.nameClass}</td>
				<td>${cl.description}</td>
				<td><a href="editclass?classId=${cl.id}">Sửa</td>
				<td><a href="deleteclass?classId=${cl.id}">Xóa</td>
			</tr>
			<br>
			
		</c:forEach>
		<div style="margin-left: 45%;">
				<button>
					<a href="addclass">Thêm lớp</a>
				</button>
		</div>
	</table>
</body>
</html>
