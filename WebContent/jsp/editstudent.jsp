<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
<head>
<meta charset="utf-8">
<title>Edit Student</title>
</head>
<body>

	<form action="editstudent" method="post">
		<h1>
			Sửa thông tin account có ID:
			<c:out value="${student.id }" />
		</h1>
		<br> ID: <input type="text" name="id"
			value="<c:out value="${student.id }"/>">
		<div></div>
		<br> Họ và tên: <input type="text" name="name"
			value="<c:out value="${student.name }"/>">
		<div></div>
		<br> Email: <input type="text" name="email"
			value="<c:out value="${student.email }"/>"><br /> <br>




		Lớp: <input type="text" name="sclass"
			value="<c:out value="${student.sclass }"/>"> <br> <br>
		
		
		Số điện thoại: <input type="text" name="tel"
			value="<c:out value="${student.tel }"/>"> <br> <br>
		Ghi chú <input type="text" name="note"
			value="<c:out value="${student.note }"/>"> <br> <br>
		<input type="submit" value="Update" />
		
		
		<br>
   		 <c:forEach items = "${listError}" var = "errMsg">
			<tr>
				<td class="errMsg" colspan="2">&nbsp;${errMsg}</td>
			</tr>
		</c:forEach>
	</form>
	
	

</body>
</html>
