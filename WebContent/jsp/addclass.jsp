<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
<head>
<meta charset="utf-8">
<title>Add Class</title>
</head>
<body>
 	
	<form action="addclass" method="post">
    	<h1>Thêm thông tin lớp </h1>
    	<br>
    	
    	
    	Tên lớp: <input type="text" name="nameClass"><div></div>
    	<br>
    	
    	Mô tả: <input type="text" name="description"><br/>
    	<br>
    	
    	<button type="submit" value="Add">Thêm lớp</button>

    	
    	<c:forEach items = "${listError}" var = "errMsg">
			<tr>
				<td class="errMsg" colspan="2" style="color: red">&nbsp;${errMsg}</td>
			</tr>
		</c:forEach>


  </form>
 
</body>
</html>
