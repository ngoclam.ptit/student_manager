<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
<head>
<meta charset="utf-8">
<title>Add account</title>
</head>
<body>
 	
	<form action="addaccount" method="post">
    	<h1>Thêm thông tin account </h1>
    	<br>
    	
    	
    	Username: <input type="text" name="username"><div></div>
    	<br>
    	
    	Password: <input type="text" name="password"><br/>
    	<br>
    	Name: <input type="text" name="name" ><div></div>
    	<br>
    	Email: <input type="text" name="email">
    	<br>
    	<br>
    	<button type="submit" value="Add">Đăng kí</button>
    	
    	<c:forEach items = "${listError}" var = "errMsg">
			<tr>
				<td class="errMsg" colspan="2" style="color: red">&nbsp;${errMsg}</td>
			</tr>
		</c:forEach>
		<c:out value="${listError}"/>

  </form>
 
</body>
</html>
