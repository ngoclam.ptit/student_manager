<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
<head>
<meta charset="utf-8">
<title>Add Student</title>
</head>
<body>

	<form action="addstudent" method="post">
		<h1>
			Thêm sinh viên
		</h1>

		<div></div>
		<br> Họ và tên: <input type="text" name="name">
		<div></div>
		<br> Email: <input type="text" name="email"><br /> <br>
		Lớp: <input type="text" name="sclass" > <br> <br>	
		Số điện thoại: <input type="text" name="tel"> <br> <br>
		Ghi chú <input type="text" name="note"> <br> <br>
		<input type="submit" value="Thêm sinh viên" />
		<br>
		<br>
		<br>
		<c:forEach items = "${listError}" var = "errMsg">
			<tr>
				<td class="errMsg" colspan="2" style="color: red">&nbsp;${errMsg}</td>
			</tr>
		</c:forEach>
		
	</form>

</body>
</html>
