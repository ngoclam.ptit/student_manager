<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
<head>
<meta charset="utf-8">
<title>Edit account</title>
</head>
<body>
 	
	<form action="editaccount" method="post">
    	<h1>Sửa thông tin account có ID: <c:out value="${account.id }"/></h1>
    	<br>
    	ID: <input type="text" name="id" value="<c:out value="${account.id }"/>"><div></div>
    	<br>
    	
    	Username: <input type="text" name="username" value="<c:out value="${account.username }"/>"><div></div>
    	<br>
    	
    	Password: <input type="text" name="password" value="<c:out value="${account.password }"/>"><br/>
    	<br>
    	Name: <input type="text" name="name" value="<c:out value="${account.name }"/>"><div></div>
    	<br>
    	Email: <input type="text" name="email" value="<c:out value="${account.email }"/>">
    	<br>
    	<br>
   		 <input type="submit" value="Update" /> 
   		 <br>
   		 <c:forEach items = "${listError}" var = "errMsg">
			<tr>
				<td class="errMsg" colspan="2">&nbsp;${errMsg}</td>
			</tr>
		</c:forEach> 	
  </form>
 
</body>
</html>
