-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2020 at 12:49 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qldt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_account`
--

CREATE TABLE `tbl_account` (
  `id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nameAcc` varchar(50) NOT NULL,
  `mailAcc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_account`
--

INSERT INTO `tbl_account` (`id`, `username`, `password`, `nameAcc`, `mailAcc`) VALUES
(2, 'admin1', 'abcd1234', 'Nguyá»n VÄn A', 'nguyenvanb@gmail.com'),
(3, 'admin3', 'abcd1234', 'Nguyễn Thị C', 'nguyenthic@gmail.com'),
(4, 'admin4', 'abcd1234', 'Nguyễn Văn D', 'nguyenvand@gmail.com'),
(6, 'admin6', 'lamkk', 'nguyá»n vÄn A', 'ngoclam@gmail.com'),
(18, 'admin2', 'abcd1234', 'lamnguyen', 'ngoclam@gmail.com'),
(19, 'adminlam', 'lam123', 'nguyá»n vÄn A', 'jerryzau@gmail.com'),
(20, 'admin111', 'abcd1234', '22', 'ngoclam@gmail.com'),
(21, 'lam', 'abcd1234', 'lamnguyen', 'ngoclam@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `id` int(10) NOT NULL,
  `nameClass` varchar(50) NOT NULL,
  `descripton` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`id`, `nameClass`, `descripton`) VALUES
(1, 'D20', 'CÃ´ng nghá» thÃ´ng tin'),
(2, 'D14', 'marketing'),
(3, 'D15', 'Điện tử viễn thông'),
(4, 'D16', 'Công nghệ thông tin'),
(5, 'D17', 'marketing'),
(6, 'D18', 'Điện tử viễn thông'),
(7, 'D19', '11111');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` int(10) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `class_id` int(10) NOT NULL,
  `note` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `fullname`, `email`, `tel`, `class_id`, `note`) VALUES
(3, 'Nguyễn Văn A', 'nguyen@gnail.com', '01245125412', 1, 'abc'),
(4, 'NguyÃ¡Â»Ân VÃÂn B', 'bngoc@gmail.com', '01245125123', 5, 'abc'),
(5, 'Nguyá»n VÄn C', 'nguyenC@gmail.com', '01245125434', 5, 'abc'),
(6, 'Nguyễn Văn D', 'bngocD@gmail.com', '01245125111', 2, 'abc'),
(8, 'Nguyễn Văn Co', 'bngoaac@gmail.com', '01245125322', 5, 'abcấ'),
(9, 'Nguyễn Văn An', 'nguyen@gmail.com', '01241225412', 4, 'abc'),
(10, 'Nguyễn Văn Baaa', 'bngoca1@gmail.com', '01249125123', 3, 'abc'),
(11, 'Nguyễn Văn Aaa', 'nguyen@gnail.com', '02245125412', 1, 'abc'),
(12, 'Nguyễn Văn Banh', 'bngoc@gmail.com', '01245725123', 5, 'abc11'),
(13, 'Nguyễn Ngọc Lâm', 'lamaa@gmailcom', '0212124511', 1, 'không có gì'),
(14, 'lamnguyen', 'ngoclam@gmail.com', '0326665656', 4, 'abc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_account`
--
ALTER TABLE `tbl_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_account`
--
ALTER TABLE `tbl_account`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD CONSTRAINT `tbl_student_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `tbl_class` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
